mod utils;
mod log;
mod objects;

use wasm_bindgen::prelude::*;
use web_sys::{console, CanvasRenderingContext2d};
use objects::Object;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub struct Game {
    frame_n: u64,
    objects: Vec<Object>,
}

#[wasm_bindgen]
impl Game {
    pub fn new() -> Self {
        Self { frame_n: 0, objects: vec![Object::new(0., 0., 20, 20)] }
    }

    pub fn get_frame_n(&self) -> u64 {
        self.frame_n
    }

    pub fn update(&self) {
        console_log!("Game updated");
    }

    pub fn render(&self, ctx: &CanvasRenderingContext2d, w: u32, h: u32) {
        for obj in &self.objects {
            ctx.fill_rect(obj.pos.x, obj.pos.y, obj.pos.w as f64, obj.pos.h as f64);
        }
    }

    pub fn key_press(&self, code: &str, char_code: u32) {
        console_log!("Key [{} - {}] pressed", char_code, code);
    }
}

#[wasm_bindgen]
pub fn init_game() -> Game {
    Game::new()
}

