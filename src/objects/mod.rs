
mod components;

use wasm_bindgen::prelude::*;

pub struct Object {
    pub pos: components::render::Position
}

impl Object {
    pub fn new(x: f64, y: f64, w: u32, h: u32) -> Self {
        Self { pos: components::render::Position { x, y, w, h } }
    }
}
