
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct Position {
    pub x: f64,
    pub y: f64,
    pub w: u32,
    pub h: u32,
}

