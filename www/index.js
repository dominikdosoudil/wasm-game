import * as wasm from "wasm-game";

const cv = document.getElementById('cv');
const ctx = cv.getContext('2d');
const cvWidth = cv.width;
const cvHeight = cv.height;

let game = wasm.init_game();
let running = true;

window.addEventListener('keypress', (e) => { game.key_press(e.code, e.charCode); })

document.getElementById('stop').addEventListener('click', () => { running = false; })
document.getElementById('start').addEventListener('click', () => { running = true; animate(); })

function animate() {
  if (running) {
//    requestAnimationFrame(animate);
  }

  ctx.clearRect(0, 0, cvWidth, cvHeight);

  ctx.fillStyle = 'palegoldenrod';

  game.render(ctx, cvWidth, cvHeight);
  //const pos = wasm.get_position();
  // ctx.fillRect(pos.x, pos.y, pos.w, pos.h);


  ctx.fillStyle = '#000';
  ctx.fillText(running ? 'game running' : 'stopped', 10, 10);

  game.update();
}

animate();

